# Snake

Not much to explain here, just a simple Snek game written in TypeScript without any libraries, frameworks, or engines.

> Play here: [antexperiments.gitlab.io/games/snake](https://antexperiments.gitlab.io/games/snake/)
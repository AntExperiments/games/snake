
import { getElement } from "./utils.js";

const canvas: HTMLCanvasElement = getElement('canvas');
const c = canvas.getContext('2d');

//#endregion
import { Game } from "./gameController.js";
export const game = new Game();
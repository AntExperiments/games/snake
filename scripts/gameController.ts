const canvas: HTMLCanvasElement = document.querySelector('canvas');
const c = canvas.getContext('2d');

import { getElement } from './utils.js';
import { Settings } from './settings.js';
import { Nom } from './partials/nom.js';
import { snek } from './partials/snek.js';


export class Game  {
    _score = 0;
    _ended = false;

    noms: Nom[] = [];

    get score() {
        return this._score;
    }

    set score(x : number) {
        this._score = x;
        getElement("#scoreboard").innerText = x;
    }

    constructor () {
        this.generateNoms();

        document.addEventListener('keydown', event => {
            if (!this._ended) {
                switch (event.code) {
                    case "KeyW":
                        snek.y--;
                        break;
                    case "KeyA":
                        snek.x--;
                        break;
                    case "KeyS":
                        snek.y++;
                        break;
                    case "KeyD":
                        snek.x++;
                        break;
                
                    default:
                        console.info(`Undefined key was pressed: '${event.code}'`)
                        break;
                }
            }
        });
    }

    generateNoms = () => {
        for (let i = 0; i < Settings.countOfNoms; i++)
            this.noms.push(new Nom());
    }

    gameEnd = () => {
        getElement("canvas").classList.add("lost")
        getElement("#gameOver").classList.add("lost")
        this._ended = true;
    }
}

//#region make canvas fill the entire screen

const fillScreen = () => {
    canvas.width = innerWidth;
    canvas.height = innerHeight;
    snek.draw();
};
window.addEventListener('resize', fillScreen);
window.addEventListener('DOMContentLoaded', fillScreen);

//#endregion
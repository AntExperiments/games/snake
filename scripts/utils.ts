const getContent = (i: string): string => (<HTMLInputElement>document.querySelector(i)).value
const getElements = (i: string): NodeList => document.querySelectorAll(i)

const sleep = (delay: number): Promise<any> => new Promise(resolve => setTimeout(resolve, delay));

// const objectsEqual = (o1, o2) => Object.keys(o1).length == Object.keys(o2).length && Object.keys(o1).every(p => o1[p] == o2[p]);

export function getElement (i: string): any { return document.querySelector(i) }
export function objectsEqual (o1, o2) { return Object.keys(o1).length == Object.keys(o2).length && Object.keys(o1).every(p => o1[p] == o2[p]) }
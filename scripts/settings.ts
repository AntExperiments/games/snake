export const Settings = {
    "countOfNoms": 5,
    "gridSize": 50,
    "gridPadding": 5,
    "headColor": "#ccc",
    "bodyColor": "#eee",
    "nomColor": "#c8e0b8"
}
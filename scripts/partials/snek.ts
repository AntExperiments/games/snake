import { Settings } from "../settings.js";
import { objectsEqual } from "../utils.js";
import { game } from '../script.js';

const canvas: HTMLCanvasElement = document.querySelector('canvas');
const c = canvas.getContext('2d');

export const snek = {
    _gridX: 3,
    _gridY: 3,
    _length: 1,
    positions: [],

    get x() { return this._gridX; },
    set x(i: number) {
        this._gridX = i;
        this.draw()
    },

    get y() { return this._gridY },
    set y(i: number) {
        this._gridY = i;
        this.draw()
    },

    get length() { return this._length },
    set length(i: number) {
        this._length = i;
        // this.draw()
    },

    draw() {
        c.clearRect(0, 0, canvas.width, canvas.height)

        // Add to positions array
        this.positions.push({x: this.x, y: this.y});
        if (this.positions.length >= this.length)
            this.positions = this.positions.slice(this.positions.length - this.length, this.positions.length)

        // check for collisions
        if (this.positions.filter(a => objectsEqual(a, this.positions[this.positions.length - 1]) && a != this.positions[this.positions.length - 1]).length)
            game.gameEnd();

        // Render Snek
        this.positions.forEach((coords, i) => {
            if (i == this.positions.length - 1) c.fillStyle = Settings.headColor;
            else        c.fillStyle = Settings.bodyColor;
            c.fillRect(
                coords.x * Settings.gridSize - Settings.gridSize / 2,
                coords.y * Settings.gridSize - Settings.gridSize / 2,
                Settings.gridSize - Settings.gridPadding,
                Settings.gridSize - Settings.gridPadding
            );
        });

        // Render Noms
        game.noms.forEach(nom => {
            if (objectsEqual(nom.position, this.positions[this.positions.length - 1])) {
                game.noms = game.noms.filter(element => element != nom)
                game.score++;
                this.length++;
            }
            else nom.draw()
        });

        // Create more Noms, if none are left
        if (game.noms.length == 0) game.generateNoms()
    }
};
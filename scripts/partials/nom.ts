import { Settings } from "../settings.js";

const canvas: HTMLCanvasElement = document.querySelector('canvas');
const c = canvas.getContext('2d');

export class Nom {
    position: { x: number, y: number };

    getRandomPosition() {
        return {
            x: Math.floor(Math.random() * Math.floor(innerWidth / Settings.gridSize) + 1),
            y: Math.floor(Math.random() * Math.floor(innerHeight / Settings.gridSize) + 1)
        };
    }

    constructor () {
        this.position = { x: this.getRandomPosition().x, y: this.getRandomPosition().y };
        this.draw();
    }

    draw() {
        c.beginPath();
        c.arc(
            this.position.x * Settings.gridSize - Settings.gridPadding/2,
            this.position.y * Settings.gridSize - Settings.gridPadding/2,
            Settings.gridSize / 2 - Settings.gridPadding,
            0,
            Math.PI * 2
        )
        c.fillStyle = Settings.nomColor;
        c.fill()
    }
};